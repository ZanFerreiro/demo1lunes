package com.captton.programa;
import java.util.*;
public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Hicimos dec de variables, constantes
		
		boolean esVaron = true;
		String name = "Mariano";
		String diaDeSemana = "Lunes";
		
		
		
		//HIcimos if, switch, case, break, for
		
		
		if(esVaron)
			System.out.println("El valor de esVaron es: verdadero");
		
		switch(diaDeSemana) {
		case "Lunes":
			System.out.println("Hoy es lunes");
			System.out.println(name);
			break;
		case "Martes":
			System.out.println("Hoy es Martes");
			break;	
		}
		
		for(int cont=0; cont <= 5; cont++) {
			System.out.println(diaDeSemana+cont);
		}
		
		//Ej con Scanner, aray y list
		
		int[] numeros = new int[5];
		ArrayList<String> alumnos = new ArrayList<>();
		
		Scanner sc = new Scanner(System.in);
		
		
		numeros[0] = 45;
		numeros[1] = 5;
		numeros[2] = 65;
		
		for(int i=0; i<5 ; i++) {
			System.out.println("Numeros en la lista: "+numeros[i]);
			
		}
		alumnos.add("Nico V");
		alumnos.add("Nico O");
		alumnos.add("Jonas");
		alumnos.add("Hans");
		alumnos.add("Etc...");
		
		for(String alu : alumnos){
			
			System.out.println(alu);
			
			if(alu.equals("Nico V"))
				System.out.println("Hola nicooo!");
			else
				System.out.println("Adios Extra�o!");
		}
		
		System.out.println("Ingrese su nombre");
		
		String nom = sc.nextLine();
		
		System.out.println("Ingrese su edad");
		
		int edad = sc.nextInt();
		
		System.out.println("Tu nombre es: "+nom+" Y tu edad es: "+edad);
		
		if(edad > 40)
			System.out.println("Que bien se te ve!");
		
		}
	}

